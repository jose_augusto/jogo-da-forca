package apresentacao;

/**
 * Classe do tipo observer que atualiza a tela para o jogador
 *
 * @author José Augusto
 * @author Marcello Victor
 * @version 0.2
 *
 * @since Release 01 da aplicação
 */
public class TelaForca {

	static TelaForca instanciaUnica;

	/**
	 * Atualiza a tela mostrando a categoria, as letras que já foram acertadas e
	 * as letras erradas.
	 *
	 * @param letrasCorretas
	 *            - é um array de letras que já foram acertadas para serem
	 *            mostradas na tela
	 * @param letrasErradas
	 *            - é um array de letras que não existe na palavra, para serem
	 *            mostradas na tela
	 * @param chances
	 *            - mostra quantas chances o usuário ainda tem
	 * @param categoria
	 *            - mostra qual é a categoria que o jogador está jogando
	 */
	public void atualizaForca(char[] letrasCorretas, char[] letrasErradas,
			int chances, String categoria) {

		limpaTela();
		System.out.println("**********  CATEGORIA " + categoria
				+ "  ************");
		imprimeLetrasCertas(letrasCorretas);
		System.out.println(' ');
		System.out.println(' ');
		imprimeLetrasErradas(letrasErradas, chances);
		System.out.println("");
		System.out.println("");

		System.out.print("Digite uma letra: ");
	}

	private void limpaTela() {
		for (int clear = 0; clear < 50; clear++) {
			System.out.println(" ");
		}
	}

	private void imprimeLetrasCertas(char[] letrasCorretas) {
		for (int i = 0; i < letrasCorretas.length; i++) {
			System.out.print(letrasCorretas[i]);
			System.out.print(' ');
			System.out.print(' ');
		}
	}

	private void imprimeLetrasErradas(char[] letrasErradas, int chances) {
		imprimeForca(chances);
		System.out.println("Você ainda pode errar " + chances + " letras");
		System.out.println("Letras erradas");
		for (int i = 0; i < letrasErradas.length; i++) {
			System.out.print(letrasErradas[i]);
			System.out.print("  ");
		}
	}

	private void imprimeForca(int chances) {
		switch (chances) {
		case 1:
			forcaCabecaTroncoPernas();
			break;
		case 2:
			forcaCabecaTroncoBracos();
			break;
		case 3:
			forcaCabecaTronco();
			break;
		case 4:
			forcaCabeca();
			break;
		case 5:
			forcaVazia();
			break;

		default:
			break;
		}
	}

	private void forcaCabecaTroncoPernas() {
		System.out.println("           ________________");
		System.out.println("           |               |");
		System.out.println("           |               |");
		System.out.println("        .--'--.            |");
		System.out.println("     ()' 0 . 0 '()         |");
		System.out.println("       '. --- .'           |");
		System.out.println("     '.   '|'   .'         |");
		System.out.println("       '...|...'           |");
		System.out.println("           |               |");
		System.out.println("          .|.              |");
		System.out.println("        .' | '.            |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("___________________________|___________");

	}

	private void forcaCabecaTroncoBracos() {
		System.out.println("           ________________");
		System.out.println("           |               |");
		System.out.println("           |               |");
		System.out.println("        .--'--.            |");
		System.out.println("        '.0 0.'            |");
		System.out.println("       '.  |  .'           |");
		System.out.println("         '.|.'             |");
		System.out.println("           |               |");
		System.out.println("           |               |");
		System.out.println("           |               |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("___________________________|___________");
	}

	private void forcaCabecaTronco() {
		System.out.println("           ________________");
		System.out.println("           |               |");
		System.out.println("           |               |");
		System.out.println("        .--'--.            |");
		System.out.println("        '.0 0.'            |");
		System.out.println("           |               |");
		System.out.println("           |               |");
		System.out.println("           |               |");
		System.out.println("           |               |");
		System.out.println("           |               |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("___________________________|___________");
	}

	private void forcaCabeca() {
		System.out.println("           ________________");
		System.out.println("           |               |");
		System.out.println("           |               |");
		System.out.println("        .--'--.            |");
		System.out.println("        '.0 0.'            |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("___________________________|___________");
	}

	private void forcaVazia() {
		System.out.println("          _________________");
		System.out.println("          |                |");
		System.out.println("          |                |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("___________________________|___________");
	}

	/**
	 * Atualiza a tela quando o usuário perde o jogo
	 *
	 * @param quantidadeErrosPossiveis
	 *            - mostra quantas letras erradas ele digitou para perder o
	 *            jogo.
	 * @param palavra
	 *            - mostra a palavra que ele não conseguiu adivinhar.
	 */
	public void atualizaDerrota(int quantidadeErrosPossiveis, String palavra) {
		limpaTela();

		System.out.println("Você perdeu, digitou " + quantidadeErrosPossiveis
				+ " letras erradas! Tente novamente");
		imprimeForcaDerrota();
		System.out.println("");
		System.out.println("A palavra correta é " + palavra);
		System.out.println("");

	}

	private void imprimeForcaDerrota() {
		System.out.println("           ________________");
		System.out.println("           |               |");
		System.out.println("           |               |");
		System.out.println("        .--'--.            |");
		System.out.println("        '.X X.'            |");
		System.out.println("           |               |");
		System.out.println("          .|.              |");
		System.out.println("        .' | '.            |");
		System.out.println("          .|.              |");
		System.out.println("        .' | '.            |");
		System.out.println("                           |");
		System.out.println("                           |");
		System.out.println("___________________________|___________");
	}

	/**
	 * Atualiza a tela quando o usuário ganha o jogo
	 *
	 * @param palavra
	 *            - mostra a palavra que ele adivinhou.
	 */
	public void atualizaVitoria(String palavra) {
		limpaTela();
		System.out.println("Você venceu, acertou a palavra: " + palavra);
		imprimeForcaVitoria();
		System.out.println("");

	}

	private void imprimeForcaVitoria() {
		System.out.println("           ________________");
		System.out.println("           |               |");
		System.out.println("           |               |");
		System.out.println("                           |");
		System.out.println("      VOCÊ VENCEU          |");
		System.out.println("                           |");
		System.out.println("        .-----.            |");
		System.out.println("        '.0 0.'            |");
		System.out.println("       '.  |  .'           |");
		System.out.println("         '.|.'             |");
		System.out.println("           |               |");
		System.out.println("          .|.              |");
		System.out.println("________.'_|_'.____________|___________");
	}

	/**
	 * Utilizado para mostrar alguma mensagem para o usuário que não esteja
	 * definida em métodos.
	 *
	 * @param mensagem
	 *            - String a ser mostrada para o usuário.
	 */
	public void mostraMensagem(String mensagem) {
		System.out.print(mensagem);

	}

	/**
	 * Imprime o menu de Escolha de Categoria:
	 *
	 * Escolha a categoria
	 *
	 * 1: Animal
	 *
	 * 2: Filme
	 *
	 * 3: Profissão
	 */
	public void imprimeMenuEscolheCategoria() {
		limpaTela();
		System.out.println("Escolha a categoria");
		System.out.println("1: Animal");
		System.out.println("2: Filme");
		System.out.println("3: Profissão");
	}

	/**
	 * Mostra que a opção digitada não é válida
	 */
	public void imprimeOpcaoValida() {
		System.out.println("Digite uma opção valida: ");
	}

	/**
	 * Imprime o menu de Escolha de Estratégia:
	 *
	 * Escolha uma estrategia para adivinhação
	 *
	 * 1: Letra por Letra
	 *
	 * 2: Todas as Letras
	 *
	 */
	public void imprimeMenuEscolheEstrategia() {
		limpaTela();
		System.out.println("Escolha uma estrategia para adivinhação");
		System.out.println("1: Letra por Letra");
		System.out.println("2: Todas as Letras");
	}

	/**
	 * Imprime o menu de Escolha de Nível:
	 *
	 * Escolha um nível:
	 *
	 * 1: Fácil
	 *
	 * 2: Médio
	 *
	 * 3: Díficil
	 */
	public void imprimeMenuEscolheNivel() {
		limpaTela();
		System.out.println("Escolha um nível:");
		System.out.println("1: Fácil");
		System.out.println("2: Médio");
		System.out.println("2: Díficil");
	}

	/**
	 * Implementação de Classe Singleton
	 *
	 *
	 * Metodo Estático para retornar uma unica instância em toda aplicação.
	 *
	 * @return TelaForca
	 *
	 */
	public static TelaForca getInstance() {
		if (instanciaUnica == null) {
			instanciaUnica = new TelaForca();
		}
		return instanciaUnica;
	}

	/**
	 * Imprime na tela que o usuário já digitou a mesma letra no modo de jogo
	 * Todas as Letras
	 */
	public void letraJaDigitada() {
		System.out.println("");
		System.out.println("Essa letra já foi digitada, digite outra!");
	}

}
