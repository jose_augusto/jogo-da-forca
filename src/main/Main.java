package main;
import java.util.Scanner;

import logica.Forca;

public class Main {
	private static Scanner n;
	private static int quantidadeJogos;

	public static void main(String[] args) {
		int opcao = 0;
		boolean fimDoJogo = false;
		while (!fimDoJogo) {
			opcao = leOpcaoMenu();
			switch (opcao) {
			case 1:
				Forca forca = new Forca();
				forca.iniciaJogo(false);
				quantidadeJogos++;
				while (!forca.getFimDoJogo()) {
					forca.capturaLetra();
				}
				break;
			case 2:
				quantidadeJogos++;
				Forca forcaRapida = new Forca();
				forcaRapida.iniciaJogo(true);
				while (!forcaRapida.getFimDoJogo()) {
					forcaRapida.capturaLetra();
				}
				break;
			case 0:
				fimDoJogo = true;
				break;
			default:
				System.out.println("Digite uma opção valida: ");
			}
		}
		System.exit(0);
	}

	private static int leOpcaoMenu() {
		System.out.println("Digite: 1 para jogar");
		if (quantidadeJogos > 0)
			System.out.println("Digite: 2 para jogar novamente");
		else
			System.out.println("Digite: 2 para jogar jogo padrão");
		System.out.println("Digite 0 para sair");
		n = new Scanner(System.in);
		return n.nextInt();
	}
}
