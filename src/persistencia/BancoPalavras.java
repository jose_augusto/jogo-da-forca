package persistencia;

/**
 * Classe utilizada para sortear palavras de três categorias, Animais, Filmes e Profissões
 *
 * @author José Augusto
 * @author Marcello Victor
 * @version 0.2
 *
 * @since Release 01 da aplicação
 */

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.util.Random;

public class BancoPalavras {

	/**
	 * Método que retorna uma palavra da categoria animais
	 *
	 *
	 * @return String - retorna uma palavra em String
	 */
	public String animais() {

		return sorteiaPalavra("animais.txt");
	}

	/**
	 * Método que retorna uma palavra da categoria filmes
	 *
	 *
	 * @return String - retorna uma palavra em String
	 */
	public String filmes() {
		return sorteiaPalavra("filmes.txt");
	}

	/**
	 * Método que retorna uma palavra da categoria filmes
	 *
	 *
	 * @return String - retorna uma palavra em String
	 */

	public String profissao() {
		return sorteiaPalavra("profissao.txt");
	}

	private String sorteiaPalavra(String arquivo) {
		InputStream inx = getClass().getResourceAsStream(arquivo);
		Reader reader = new java.io.InputStreamReader(inx);
		BufferedReader lerArq = new java.io.BufferedReader(reader);

		File arquivoLeitura = new File(getCleanPath("src/persistencia/"
				+ arquivo));

		// pega o tamanho
		long tamanhoArquivo = arquivoLeitura.length();

		FileInputStream fs = null;
		try {
			fs = new FileInputStream(arquivoLeitura);

			DataInputStream in = new DataInputStream(fs);

			LineNumberReader lineRead = new LineNumberReader(
					new InputStreamReader(in));

			lineRead.skip(tamanhoArquivo);
			int num_linhas = lineRead.getLineNumber() + 1;
			int num_sorteado = sorteiaNum(num_linhas);
			for (int i = 0; i < num_linhas; i++) {
				String palavra = lerArq.readLine();
				if (i == num_sorteado)
					return palavra;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String getCleanPath(String path) {
		ClassLoader classLoader = BancoPalavras.class.getClassLoader();
		File classpathRoot = new File(classLoader.getResource("").getPath());

		return classpathRoot.getPath().substring(0,
				classpathRoot.getPath().length() - 3)
				+ path;
	}

	private int sorteiaNum(int quant_linhas) {
		Random gerador = new Random();
		return gerador.nextInt(quant_linhas);
	}
}
