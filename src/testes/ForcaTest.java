package testes;

import static org.junit.Assert.*;
import logica.Forca;

import org.junit.Test;

public class ForcaTest {
	Forca forca;
	@Test
	public void forcaInicial() {
		this.forca = new Forca();
		assertEquals(false, forca.getFimDoJogo());
		assertEquals(null, forca.getPalavra());
		assertEquals(1, forca.getTipoCategoria());
		assertEquals("todasAsLetras", forca.getTipoDeEstrategia());
	}

	@Test
	public void forcaInterator(){
		this.forca = new Forca();
		forca.setPalavra("ANIMAL");
		assertEquals(-1, forca.getPosicao());
		int i = -1;
		while(forca.getPosicao() < 3){
			if(forca.hasNext()){
				i++;
				forca.next();
			}
		}
		assertEquals(i, forca.getPosicao());
		char letra = forca.getPalavra().charAt(forca.getPosicao()-1);
		assertEquals(letra, "I".charAt(0));
	}

}
