package logica;

/**
 * Classe que implementa a classe abstrata Niveis
 *
 * @author José Augusto
 * @author Marcello Victor
 * @version 0.2
 *
 * @since Release 01 da aplicação
 */
public class Medio implements Niveis {

	/**
	 * Método que retorna nível médio que possibilita 7 erros
	 * @return int - o valor retornado é 7
	 */
	@Override
	public int nivel() {
		return 7;
	}

}
