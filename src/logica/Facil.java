package logica;

/**
 * Classe que implementa a classe abstrata Niveis
 *
 * @author José Augusto
 * @author Marcello Victor
 * @version 0.2
 *
 * @since Release 01 da aplicação
 */

public class Facil implements Niveis{

	/**
	 * Método que retorna nível facil que possibilita 14 erros
	 * @return int - o valor retornado é 14
	 */

	@Override
	public int nivel() {
		return 14;
	}

}
