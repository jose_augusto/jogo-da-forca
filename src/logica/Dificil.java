package logica;

/**
 * Classe que implementa a classe abstrata Niveis
 *
 * @author José Augusto
 * @author Marcello Victor
 * @version 0.2
 *
 * @since Release 01 da aplicação
 */
public class Dificil implements Niveis {

	/**
	 * Método que retorna nível dificil que possibilita 5 erros
	 * @return int - o valor retornado é 5
	 */
	@Override
	public int nivel() {
		return 5;
	}

}
