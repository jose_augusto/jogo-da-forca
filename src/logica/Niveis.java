package logica;

/**
 * Interface utilizada para criar o padrão de projeto Strategy
 *
 * @author José Augusto
 * @author Marcello Victor
 * @version 0.2

 * @since Release 01 da aplicação

 */
public interface Niveis {
	int nivel();
}
