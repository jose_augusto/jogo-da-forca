package logica;

interface Iterator {
	boolean hasNext();
	char next();
}
