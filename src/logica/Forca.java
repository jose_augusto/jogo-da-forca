package logica;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import apresentacao.TelaForca;
import persistencia.BancoPalavras;

/**
 * Classe utilizada para gerenciar o jogo da forca
 *
 * @author José Augusto
 * @author Marcello Victor
 * @version 0.2
 *
 * @since Release 01 da aplicação
 */

public class Forca implements Iterator {
	private static Scanner n;
	private BancoPalavras bancoPalavras;
	TelaForca tela;
	private String palavra;
	private int niveis;
	private int conta_erros;
	private int conta_correta;
	private char[] letrasCorretas;
	private char[] letrasErradas;
	private String arrLetrasErradas;
	private String arrLetrasCertas;
	private boolean fimDoJogo;
	private int tipoCategoria = 1;
	private String tipoDeEstrategia;
	private int posicao = -1;

	/** Contructor da Classe Forca */
	public Forca() {
		this.tela = TelaForca.getInstance();
		this.bancoPalavras = new BancoPalavras();
		this.niveis = new Dificil().nivel();
		this.tipoDeEstrategia = "todasAsLetras";
		this.arrLetrasCertas = new String();
		this.arrLetrasErradas = new String();
	}

	/**
	 * Método que inicia o jogo, seja ele com a inicialização padrão ou para
	 * possiveis configurações de categoria, tipo de estrategia e níveis fácil,
	 * médio e dificíl.
	 *
	 * @param inicializacaoPadrao
	 *            - parametro do tipo boolean que indica se o jogo vai ser
	 *            inicializado o jogo padrão ou se vai ser configurado.
	 */
	public void iniciaJogo(boolean inicializacaoPadrao) {
		try {
			inicializaPropriedades();
			if (!inicializacaoPadrao) {
				tela.imprimeMenuEscolheCategoria();
				escolheCategoria();
			} else
				sorteiaPalavra(getTipoCategoria(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Método que inicializa as propriedades padrões do jogo.
	 */
	public void inicializaPropriedades() {
		fimDoJogo = false;
		conta_erros = -1;
		conta_correta = 0;
		letrasErradas = new char[niveis];
	}

	/**
	 * Espera a opção da categoria ser digitada do usuário.
	 */
	private void escolheCategoria() throws IOException {
		n = new Scanner(System.in);
		sorteiaPalavra(n.nextInt(), false);
	}

	/**
	 * Sorteia a palavra para inicar o jogo.
	 *
	 *
	 * @param nextInt
	 *            - é o qual categoria o usuário escolheu, sendo:
	 *
	 *
	 *            1 - Animal
	 *
	 *
	 *            2 - Filmes
	 *
	 *
	 *            3 - Profissões
	 *
	 *
	 * @param jogarNovamente
	 *            - é um tipo boolean que é passado para saber se o usuário quer
	 *            jogar a última configuração jogada se for true, o jogo é
	 *            inicado a partir do ultimo jogo, níveis, categoria e
	 *            estratégia.
	 */
	private void sorteiaPalavra(int nextInt, boolean jogarNovamente)
			throws IOException {
		if (!jogarNovamente) {
			escolheTipoEstrategia();
			escolheNivel();
		} else
			tipoDeEstrategia = "todasAsLetras";
		switch (nextInt) {
		case 1:
			setPalavra(bancoPalavras.animais());
			setTipoCategoria(nextInt);
			break;
		case 2:
			setPalavra(bancoPalavras.filmes());
			setTipoCategoria(nextInt);
			break;
		case 3:
			setPalavra(bancoPalavras.profissao());
			setTipoCategoria(nextInt);
			break;
		default:
			tela.imprimeOpcaoValida();
			escolheCategoria();
			break;
		}
		inicializaLetrasCorretas();
		imprimeLetras();
	}

	/**
	 * Escolhe o nível a ser jogado, fácil, médio e dificil. Esse método seta
	 * uma propriedade da classe chamada letrasErradas, onde cada nível retorna
	 * uma quantidade de erros possíveis para cada nível.
	 *
	 * Opções no menu: 1 - Fácil 2 - Médio 3 - Dificil
	 */
	private void escolheNivel() {
		tela.imprimeMenuEscolheNivel();

		n = new Scanner(System.in);

		switch (n.nextInt()) {
		case 1:
			niveis = new Facil().nivel();
			letrasErradas = new char[niveis];
			break;
		case 2:
			niveis = new Medio().nivel();
			letrasErradas = new char[niveis];
			break;
		case 3:
			niveis = new Dificil().nivel();
			letrasErradas = new char[niveis];
			break;
		default:
			tela.imprimeOpcaoValida();
			escolheTipoEstrategia();
			break;
		}

	}

	/**
	 * Escolhe o tipo de estratégia a ser utilizado.
	 *
	 *
	 * Opções no menu:
	 *
	 *
	 * 1 - Letra por Letra
	 *
	 *
	 * 2 - Todas as Letras
	 *
	 *
	 * Na estratégia Letra por Letra o usuário digita uma letra e é mostrada
	 * somente a primeira vez que ela aparece se o jogador digitar ela novamente
	 * e tiver uma proxima letra a letra é mostrada, se não tiver o usuário
	 * perde uma chance.
	 *
	 *
	 * Na estratégia Todas as Letras o usuário digita uma letra e é mostrada
	 * todas as letras encontradas na palavra se o usuário digitar ela novamente
	 * ela só é ignorada.
	 */
	private void escolheTipoEstrategia() {
		tela.imprimeMenuEscolheEstrategia();

		n = new Scanner(System.in);

		switch (n.nextInt()) {
		case 1:
			setTipoDeEstrategia("letraPorLetra");
			break;
		case 2:
			setTipoDeEstrategia("todasAsLetras");
			break;
		default:
			tela.imprimeOpcaoValida();
			escolheTipoEstrategia();
			break;
		}
	}

	/**
	 * Esse método preenche uma propriedade do tipo array com "_" pela
	 * quantidade de letras que a palavra tem.
	 *
	 *
	 * Exemplo:
	 *
	 *
	 * A palavra Cachorro, é preenchido nesse arrar um array com 8 possições que
	 * contem uma string assim "_".
	 */
	private void inicializaLetrasCorretas() {
		this.letrasCorretas = new char[getPalavra().length()];
		while (hasNext()) {
			if (next() == ' ') {
				preencheLetra(getPosicao(), getPalavra().charAt(getPosicao()));
				arrLetrasCertas += getPalavra().charAt(getPosicao());
			} else {
				this.letrasCorretas[getPosicao()] = "_".charAt(0);
			}
		}
	}

	/**
	 * Quando é digitada uma letra esse método verifica se a letra possui na
	 * palavra, e antes dele preencher o array de letras corretas ele verifica
	 * qual o tipo de estratégia utilizada.
	 *
	 *
	 * Após isso se não tiver a letra na palavra ele preenche um array de letras
	 * erradas, se estiver correta ela preenche o array de letras corretas.
	 */
	public void verificaLetra(char letra) {
		if (getTipoDeEstrategia().equals("todasAsLetras"))
			verificaTodasAsLetras(letra);
		else
			verificaLetraPorLetra(letra);

		if (arrLetrasErradas.equals("")) {
			if (getPalavra().indexOf(letra) == -1) {
				arrLetrasErradas += letra;
				preencheLetrasErradas(letra);
			}
		} else {
			for (int i = 0; i < arrLetrasErradas.length(); i++) {
				if (arrLetrasErradas.indexOf(letra) == -1) {
					if (arrLetrasCertas.indexOf(letra) == -1) {
						preencheLetrasErradas(letra);
						arrLetrasErradas += letra;
					}
				} else
					imprimeLetras();
			}
		}
	}

	/**
	 * Verifica a palavra letra por letra, quando é achado uma letra que não foi
	 * mostrada ainda ela preenche a letra e finaliza o ciclo do while, caso não
	 * tenha mais essa letra ele preenche as letras erradas.
	 */
	private void verificaLetraPorLetra(char letra) {
		setPosicao(-1);
		boolean naoTemMaisLetras = false;
		while (hasNext()) {
			if (next() == letra) {
				if (letrasCorretas[getPosicao()] != letra) {
					preencheLetra(getPosicao(), letra);
					arrLetrasCertas += letra;
					naoTemMaisLetras = false;
					break;
				} else
					naoTemMaisLetras = true;
			}
		}
		if (naoTemMaisLetras)
			preencheLetrasErradas(letra);
	}

	/**
	 * Verifica e preenche todas as letras digitas na palavra. E preenche o
	 * array de letras corretas.
	 */
	private void verificaTodasAsLetras(char letra) {
		setPosicao(-1);
		while (hasNext()) {
			if (next() == letra) {
				preencheLetra(getPosicao(), letra);
				arrLetrasCertas += letra;
			}
		}
	}

	/**
	 * Quando é digitado uma letra que não possui na palavra é adicionado no
	 * array de letras erradas e é contado um erro cada vez que se entra nessa
	 * função, sendo que quando o jogador digitar uma quantidade de letras
	 * errada dependendo do nível escolhido o jogo é finalizado.
	 */
	private void preencheLetrasErradas(char letra) {
		conta_erros++;
		if (conta_erros < niveis - 1) {
			letrasErradas[conta_erros] = letra;
			imprimeLetras();
		} else {
			letrasErradas[conta_erros] = letra;
			finalizaJogo(false);

		}
	}

	/**
	 * O jogo pode ser finalizado de duas formas, ganhando ou perdendo.
	 *
	 *
	 * @param vitoria
	 *            - É o tipo boolena, quando é passado true, quer dizer que o
	 *            jogador venceu, ai ela avisa para a class TelaForca que o
	 *            usuário venceu e que é para ele mostrar issa informação na
	 *            tela. Quando é passado false, quer dizer que o jogado perdeu,
	 *            e ele é avisa da mesma forma através da TelaForca.
	 */
	private void finalizaJogo(boolean vitoria) {
		if (!vitoria) {
			tela.atualizaDerrota(niveis, getPalavra());
			setFimDoJogo();
		} else {
			tela.atualizaVitoria(getPalavra());
			setFimDoJogo();
		}
	}

	/**
	 * Seta o fim do jogo, quando o usuário acerta ou erra a palavra que está
	 * sendo adivinhada
	 */
	public void setFimDoJogo() {
		this.fimDoJogo = true;
	}

	/**
	 * Utilizado para saber se o jogo foi finalizado é utilizado no while no
	 * main para continuar a pegar letras digitadas.
	 */
	public boolean getFimDoJogo() {
		return this.fimDoJogo;
	}

	/**
	 * Método que envia informações para a tela mostrar quais as letras ja
	 * acertadas, erradas, o nível, quantidade de erros e a categoria.
	 */
	private void imprimeLetras() {
		String categoria = "";
		switch (getTipoCategoria()) {
		case 1:
			categoria = "ANIMAIS";
			break;
		case 2:
			categoria = "FILMES";
			break;
		case 3:
			categoria = "PROFISSÕES";
			break;
		}
		if (!fimDoJogo)
			tela.atualizaForca(letrasCorretas, letrasErradas, niveis
					- conta_erros - 1, categoria);
	}

	private void preencheLetra(int i, char c) {
		if (letrasCorretas[i] != c) {
			letrasCorretas[i] = c;
			imprimeLetras();
			conta_correta++;
			if (this.conta_correta == getPalavra().length())
				finalizaJogo(true);
		}
		else{
			imprimeLetras();
			tela.letraJaDigitada();
		}
	}

	/**
	 * Captura tela é utilizado para capturar o que o usuário digita.
	 *
	 *
	 * Ele não aceita
	 *
	 *
	 * * números quando
	 *
	 *
	 * * mais de uma letra
	 *
	 *
	 * Isso ocorre se o jogador está adivinhando a palavra. Depois de capturar a
	 * letra ele manda verificar se a letra existe.
	 *
	 */
	public void capturaLetra() {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String letra = null;
		try {

			boolean eNumero = false;
			letra = in.readLine();
			if (letra.length() == 1) {
				for (int i = 0; i < 10; i++) {
					String x = new String();
					x = "" + i + "";
					if (letra.indexOf(x) != -1) {
						eNumero = true;
						tela.mostraMensagem("Digite apenas letras: ");
					}
				}
				if (!eNumero && !letra.equals("")) {
					letra = letra.toUpperCase();
					verificaLetra(letra.charAt(0));
				} else {
					imprimeLetras();
				}
			} else {
				tela.mostraMensagem("Digite apenas uma letra: ");
				capturaLetra();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Pega a palavra que está sendo adivinhada.
	 *
	 * @return palavra
	 */
	public String getPalavra() {
		return palavra;
	}

	/**
	 * Seta a palavra que vai ser adivinhada.
	 */
	public void setPalavra(String palavra) {
		this.palavra = palavra;
	}

	/**
	 * Pega a tipo de categoria utilizada.
	 *
	 * @return tipoCategoria
	 */
	public int getTipoCategoria() {
		return tipoCategoria;
	}

	/**
	 * Seta a o tipo de categoria que vai ser adivinhado.
	 */
	public void setTipoCategoria(int tipoCategoria) {
		this.tipoCategoria = tipoCategoria;
	}

	/**
	 * Pega a tipo de estratégia a ser utilizado.
	 *
	 * @return tipoDeEstrategia
	 */
	public String getTipoDeEstrategia() {
		return tipoDeEstrategia;

	}

	/**
	 * Seta a o tipo de estratégia a ser utilizado.
	 */
	public void setTipoDeEstrategia(String tipoDeEstrategia) {
		this.tipoDeEstrategia = tipoDeEstrategia;
	}

	/**
	 * Método utilizado para saber se há uma proxima letra na palavra.
	 *
	 *
	 * @return true - Caso exista uma proxima letra
	 *
	 *
	 * @return false - Caso não exista uma proxima letra
	 */
	@Override
	public boolean hasNext() {
		if (getPosicao() + 1 >= palavra.length())
			return false;
		else
			return true;
	}

	/**
	 * Metódo utilizado para pegar a proxima letra.
	 *
	 * @return letra
	 */
	@Override
	public char next() {
		setPosicao(getPosicao() + 1);
		char letra = palavra.charAt(getPosicao());
		return letra;
	}

	/**
	 * Pega a posição que da letra que se está percorrendo no momento
	 *
	 *
	 * @return posicao
	 */
	public int getPosicao() {
		return posicao;
	}

	/**
	 * Seta a posição da letra.
	 */
	public void setPosicao(int posicao) {
		this.posicao = posicao;
	}

}
